# 1.0.1.0
- add Timestamp conversion from/to UTCTime
- add replyTo, reactTo

# 1.0.0.2
- add version bounds on deps
- add changelog

# 1.0.0.1
- add more package metadata

# 1.0.0.0
- initial stable release
