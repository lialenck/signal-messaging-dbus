# signal-messaging-dbus
Library for connecting a haskell program to a [signal-cli](https://github.com/AsamK/signal-cli)-instance running in daemon mode, using its DBus interface.

Support exists for almost the whole api, which should be sufficient for your bot-writing needs.

The library is fully documented with haddock, and mostly tested.
You can either build the documentation yourself with `stack haddock --open signal-messaging-dbus`, or look at it on hackage.  
For an a small code example, see `example/Main.hs`.
