{-# LANGUAGE LambdaCase #-}

module Main (main) where

import Control.Monad (forever, when)
import SignalDBus

reactWithSkull :: Timestamp -> String -> Maybe Group -> SignalConn -> IO Timestamp
reactWithSkull ts n mayG sc =
    case mayG of
        Nothing -> sendMessageReaction "💀" False n ts n sc
        Just g -> sendGroupMessageReaction "💀" False n ts g sc

-- react with 💀 to every message received by a specific user
main :: IO ()
main = do
    let targetNumber = "[insert phone number with country code]"

    withConn $ \sc -> do
        myNumber <- getSelfNumber sc
        putStrLn $ "Running on: " ++ show myNumber

        withReceiveMessages sc $ \getMsg -> forever $ do
            getMsg >>= \case
                Message ts n g _ _ -> when (n == targetNumber) $ do
                    reactWithSkull ts targetNumber g sc
                    return ()
                _ -> return ()

    return ()
