{-# OPTIONS_GHC -Wno-orphans #-} -- :/
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

{-|
Module:      SignalDBus.Types
Description: Types used by this package
Copyright:   (c) Lia Lenckowski, 2022
License:     AGPL
Maintainer:  lialenck@protonmail.com
Stability:   experimental
Portability: GNU/Linux, MacOS

This module contains types used by this package. Please import SignalDBus instead.
-}

module SignalDBus.Types (
    SignalConn(..),
    Timestamp,
    Device(..),
    Group(..),
    ReceivedMessage(..),

    toUTCTime,
    fromUTCTime,
) where

import Data.Int (Int64)
import Data.Time (UTCTime)
import Data.Time.Clock.POSIX (posixSecondsToUTCTime, utcTimeToPOSIXSeconds)
import DBus.Client (Client)
import DBus.Internal.Types
import Text.Read (readPrec)

-- |Opaque connection object, aquired by 'SignalDBus.withConn' or
-- 'SignalDBus.withConnNum'
data SignalConn = SignalConn !ObjectPath !Client

-- |Timestamp, represented as an ms-precision unix timestamp
newtype Timestamp = Timestamp Int64 deriving (Show, Read, Eq, Ord, IsVariant, IsValue)
-- |Conversion to 'UTCTime'. This retains precision, so converting back-and-forth
-- is unproblematic.
toUTCTime :: Timestamp -> UTCTime
toUTCTime (Timestamp ts) = posixSecondsToUTCTime (fromIntegral ts * 0.001)
-- |Conversion from 'UTCTime'. This retains precision, so converting back-and-forth
-- is unproblematic.
fromUTCTime :: UTCTime -> Timestamp
fromUTCTime t = Timestamp $ floor $ utcTimeToPOSIXSeconds t * 1000

-- |Opaque object representing a linked device
newtype Device = Device Int64 deriving (Show, Read, Eq, IsVariant, IsValue)

instance Read ObjectPath where -- :/
    readPrec = ObjectPath <$> readPrec

-- |Opaque Group object, aquired by 'SignalDBus.listGroups' or 'SignalDBus.getGroup'
newtype Group = Group ObjectPath deriving (Show, Read, Eq, Ord, IsVariant, IsValue)

-- |Received message
data ReceivedMessage =
    SyncMessage       -- ^ Message sent by a linked device to someone else
        Timestamp     -- ^ When this message was sent
        String        -- ^ Message sender (TODO: always yourself?)
        (Maybe Group) -- ^ If sent in a group, corresponding group object
        String        -- ^ Message text
        [String]      -- ^ Paths to stored attachments

  | Receipt       -- ^ Read receipt sent by someone else in response to one of your messages
        Timestamp -- ^ Which message was read
        String    -- ^ Phone number of the receipt sender

  | Message           -- ^ Message sent to you by someone else
        Timestamp     -- ^ When this message was sent
        String        -- ^ Message sender
        (Maybe Group) -- ^ If sent in a group, corresponding group object
        String        -- ^ Message text
        [String]      -- ^ Paths to stored attachments

    deriving (Show, Read, Eq, Ord)
